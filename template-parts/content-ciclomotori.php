<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alba_chiara
 */
$subtitle = get_field('subtitle');
$text = get_field('text');
?>

<header>
</header><!-- .entry-header -->
<article id="post-<?php the_ID(); ?>" class="c-article l-container">

	<div class="entry-content">
	<div>
		<?php echo the_title( '<h1 class="c-mopedDetail__title">', '</h1>' ); ?>
		<h2 class="c-mopedDetail__subtitle"><?php echo $subtitle ?></h2>
		<br>
		<?php if ($text) : ?>
			<div class="u-left-line"><?php echo $text ?></div>
		<?php endif ?>
		<?php
		if( have_rows('rowprice') ):
			while ( have_rows('rowprice') ) : the_row(); 
			$inforRow = get_sub_field('group'); ?>	
				<p class="c-mopedDetail__row alternate u-left-line">
					<span><?php echo $inforRow['description']; ?></span> <strong><?php echo $inforRow['price']; ?></strong></p>
			<?php endwhile;
		endif;
		?>
	</div>
	
	<div class="c-mopedDetail__image">
		<?php the_post_thumbnail('full'); ?>
	</div>
	<a href="#colophon" class="o-button js-close">Noleggia</a>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
