<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alba_chiara
 */

?>

<header class="c-article__header">
	<div class="c-article__image">
		<div class="u-cover-image">
			<?php echo the_post_thumbnail(); ?>
		</div>
	</div>
	<div class="c-article__title">
		<?php if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php echo get_the_date(); ?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
		<div class="c-article__category">
			<?php echo the_category(); ?>
		</div>
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="title">', '</h1>' );
		else :
			the_title( '<h2 class="title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>
	</div>
</header><!-- .entry-header -->
<article id="post-<?php the_ID(); ?>" class="c-article l-container">

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'alba-chiara-moto-noleggio' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'alba-chiara-moto-noleggio' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->
	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'alba-chiara-moto-noleggio' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					false
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
