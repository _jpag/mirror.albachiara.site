<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alba_chiara
 */

?>

<article id="post-<?php the_ID(); ?>" class="c-blog__item big">
	<div class="c-blogitem__content">
		<div class="c-blogitem__category">
			<?php echo the_category() ?>
		</div>
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="c-blogitem__title">', '</h1>' );
		else :
			the_title( '<h2 class="c-blogitem__title">', '</h2>' );
		endif;?>

		<div class="c-blogitem__text">
			<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'alba-chiara-moto-noleggio' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );
			?>
		</div>
		<a class="c-blogitem__link" href="<?php echo get_post_permalink() ?>"></a>
	</div>
	
	<div class="c-blogitem__image">
		<div class="u-cover-image">
			<?php echo the_post_thumbnail(); ?>
		</div>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
