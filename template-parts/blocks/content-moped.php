<?php
	/**
	 * Block Name: Mopeds carousel
	 */
    $title = get_field('title');
    $text = get_field('text');
    $link1 = get_field('link_1');
    $link2 = get_field('link_2');
    $linkPage = get_field('link_page');
    $delay = 50;
?>

<section class="c-mopedscarousel u-deco-square">
    <div class="c-mopedscarousel__header l-container">
        <?php if ($title): ?>
            <h2 class="c-mopedscarousel__title"><?php echo $title ?></h2>
        <?php endif ?>  
        <?php if ($text): ?>
            <div class="c-mopedscarousel__text u-left-line">
                <?php echo $text ?>
            </div>
        <?php endif ?>  
        <div class="c-mopedscarousel__actions">
            <?php if ($link1): ?>
                <a class="o-button js-anchorScroll" target="<?php echo $link1['target'] ?>" href="<?php echo $link1['url'] ?>"><?php echo $link1['title'] ?></a>
            <?php endif ?>
            <?php if ($link2): ?>
                <a class="o-button js-anchorScroll" target="<?php echo $link2['target'] ?>" href="<?php echo $link2['url'] ?>"><?php echo $link2['title'] ?></a>
            <?php endif ?>
        </div>
    </div>
    <div class="c-mopedscarousel__inner owl-carousel js-carouselHome">
    <?php
		$query = new WP_Query( array('posts_per_page' => 99, 'post_type' => 'ciclomotori' ) );
		if ( $query->have_posts() ):
            while ( $query->have_posts() ) : $query->the_post();?>	
				<div class="c-moped js-appear" js-delay="<?php echo $delay ?>">
                    <?php echo the_post_thumbnail('medium') ?>
                    <div class="c-moped__name">
                        <?php echo get_the_title() ?>
                    </div>
				</div>
                <?php
            $delay += 20;
			endwhile;
		endif;
	?>
    </div>
</section>