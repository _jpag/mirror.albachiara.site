<?php 
    $title = get_field('title');
    $text = get_field('text');
    $link = get_field('link');
    $image1 = get_field('image_1');
    $image2 = get_field('image_2');
    $image3 = get_field('image_3');
?>

<section class="c-teaserblog u-deco-square">
    <div class="c-teaserblog__content l-container">
        <?php if ($title): ?>
            <h2 class="c-teaserblog__title"><?php echo $title ?></h2>
        <?php endif ?>
        <?php if ($text): ?>
            <div class="c-teaserblog__text u-left-line">
                <?php echo $text ?>
            </div>
        <?php endif ?>
        <?php if ($link): ?>
            <a class="o-button" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
        <?php endif ?>
    </div>
    <div class="c-teaserblog__images l-container owl-carousel js-carouselBlog">
        <?php if ($image1): ?>
            <img class="js-appear" js-delay="50" src="<?php echo $image1['url'] ?>" alt="<?php echo $image1['alt'] ?>">
        <?php endif ?>
        <?php if ($image2): ?>
            <img class="js-appear" js-delay="70" src="<?php echo $image2['url'] ?>" alt="<?php echo $image2['alt'] ?>">
        <?php endif ?>
        <?php if ($image3): ?>
            <img class="js-appear" js-delay="90" src="<?php echo $image3['url'] ?>" alt="<?php echo $image3['alt'] ?>">
        <?php endif ?>
    </div>
</section>