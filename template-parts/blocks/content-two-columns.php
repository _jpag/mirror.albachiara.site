<?php if(get_field('columns')): 
    $delay = 50;
?>
<section class="c-columns l-container">
    <?php while(has_sub_field('columns')): 
            $title = get_sub_field('title');
            $link = get_sub_field('link');	
        ?>
        <div class="c-columns__2col js-appear" js-delay="<?php echo $delay ?>">
            <h3 class="c-column__title u-left-line"><?php echo $title ?></h3>
            <?php if ($link): ?>
                <a class="o-button js-anchorScroll" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
            <?php endif ?>  
        </div>
    <?php 
        $delay += 40;
        endwhile; 
    ?>
</section>
<?php endif; ?>