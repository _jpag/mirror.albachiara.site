<?php 
    $title = get_field('title');
    $text = get_field('text');
    $instagram = get_field('instagram');
    $facebook = get_field('facebook');
    $form_title = get_field('form_title');
    $form_text = get_field('form_text');
?>

<section class="c-contact l-container">
    <div class="c-contact__col col1 js-appear">
        <?php if ($title): ?>
            <h1 class="c-contact__title"><?php echo $title ?></h1>
        <?php endif ?>  
        <?php if ($text): ?>
            <?php echo $text ?>
        <?php endif ?>  
        <div class="c-contact__social">
            <?php if ($facebook): ?>
            <a href="<?php echo $facebook['url'] ?>" target="<?php echo $facebook['url'] ?>">
                <img class="social-icon" src="<?php echo get_template_directory_uri() ?>/images/social-facebook-blu.svg" alt="Alba Chiara Noleggio Facebook">
            </a>
            <?php endif ?>  

            <?php if ($instagram): ?>
                <a href="<?php echo $instagram['url'] ?>" target="<?php echo $instagram['target'] ?>">
                    <img class="social-icon" src="<?php echo get_template_directory_uri() ?>/images/social-instagram-blu.svg" alt="Alba Chiara Noleggio Instagram">
                </a>
            <?php endif ?>  
        </div>
    </div>
    <div class="c-contact__col col2 js-appear" js-delay="30">
        <?php if ($form_title): ?>
            <h2 class="c-contact__title"><?php echo $form_title ?></h2>
        <?php endif ?>  
        <?php if ($form_text): ?>
            <?php echo $form_text ?>
        <?php endif ?> 
    </div>
</section>