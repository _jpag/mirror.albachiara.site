<?php 
    $subtitle = get_field('subtitle');
    $title = get_field('title');
    $text = get_field('text');
    $image = get_field('image');
?>

<section class="c-homerow">
    <div class="c-homerow__image" style="background-image:url('<?php echo $image ?>')"></div>
    <div class="c-homerow__content js-appear">
        <?php if ($subtitle): ?>
            <h4 class="c-homerow__subtitle"><?php echo $subtitle ?></h4>
        <?php endif ?>  
        <?php if ($title): ?>
            <h3 class="c-homerow__title"><?php echo $title ?></h3>
        <?php endif ?>  
        <?php if ($text): ?>
            <?php echo $text ?>
        <?php endif ?>  
    </div>
</section>