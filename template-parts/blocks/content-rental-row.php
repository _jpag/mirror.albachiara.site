<?php 
    $subtitle = get_field('subtitle');
    $title = get_field('title');
    $text = get_field('text');
    $image = get_field('image');
?>

<section class="c-rentalrow">
    <div class="c-rentalrow__image" style="background-image:url('<?php echo $image ?>')"></div>
    <div class="c-rentalrow__content l-container">
        <div class="c-rentalrow__inner">
        <?php if ($subtitle): ?>
            <h4 class="c-rentalrow__subtitle"><?php echo $subtitle ?></h4>
        <?php endif ?>  
        <?php if ($title): ?>
            <h3 class="c-rentalrow__title"><?php echo $title ?></h3>
        <?php endif ?>  
        <?php if ($text): ?>
            <div class="u-left-line">
            <?php echo $text ?>
            </div>
        <?php endif ?>  
        </div>
    </div>
</section>