<?php 
    $title = get_field('title');
    $subtitle = get_field('subtitle');
    $text = get_field('text');
?>

<section class="c-teasercontact l-container">
    <?php if ($title): ?>
        <h2 class="c-teasercontact__title"><?php echo $title ?></h2>
    <?php endif ?>  
    <div class="c-teasercontact__content">
        <?php if ($text): ?>
            <div class="c-teasercontact__text u-left-line"><?php echo $text ?></div>
        <?php endif ?>  
        <?php if ($subtitle): ?>
            <h3 class="c-teasercontact__subtitle"><?php echo $subtitle ?></h3>
        <?php endif ?>  
    </div>
</section>