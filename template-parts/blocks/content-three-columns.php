<?php if(get_field('columns')): 
    $delay = 50;
?>
<section class="c-columns__bg">
    <div class="c-columns l-container">
    <?php while(has_sub_field('columns')): 
            $title = get_sub_field('title');
            $text = get_sub_field('text');
        ?>
        <div class="c-columns__3col js-appear" js-delay="<?php echo $delay ?>">
            <h3 class="c-column__title"><?php echo $title ?></h3>
            <div>
                <?php echo $text ?>
            </div>
        </div>
    <?php 
        $delay += 40;
        endwhile; 
    ?>
    </div>
</section>
<?php endif; ?>