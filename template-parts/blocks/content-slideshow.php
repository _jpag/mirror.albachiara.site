<?php
	/**
	 * Block Name: Showreel
	 */
	$count = 0;
?>
<?php if(get_field('slideshow')): ?>
	<section class="c-slideshow">
		<?php while(has_sub_field('slideshow')): ?>
			<div class="c-slideshow__slide item">
				<?php 
					$pretitle = get_sub_field('pretitle');
                    $title = get_sub_field('title');
                    $text = get_sub_field('text');
					$image = get_sub_field('image');
					$link = get_sub_field('link');
					$alt = array('alt'=>$title);
				?>
				<div class="c-slideshow__image">
					<div class="u-cover-image">
						<?php if( $image ) {
							echo wp_get_attachment_image($image, 'large', false, $alt);
						} ?>
					</div>
				</div>

				<div class="c-slideshow__content l-container">
					<div class="inner">
						<?php if ($pretitle): ?>
							<h4 class="c-slideshow__pretitle"><?php echo $pretitle ?></h4>
						<?php endif ?>
						<?php if ($title): ?>
							<h2 class="c-slideshow__title"><?php echo $title ?></h2>
						<?php endif ?>  
						<?php if ($text): ?>
							<?php echo $text ?>
						<?php endif ?>  
						<?php if ($link): ?>
							<a class="o-button white js-anchorScroll" target="<?php echo $link['target'] ?>" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
						<?php endif ?>
					</div>
				</div>
			</div>
			<?php $count++ ?>
		<?php endwhile; ?>
		
		<?php if ($count > 1): ?>
			<div class="c-slideshow__pager"></div>
		<?php endif ?>

	</section>
<?php endif; ?>

