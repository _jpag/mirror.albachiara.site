<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package alba_chiara
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function alba_chiara_moto_noleggio_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'alba_chiara_moto_noleggio_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function alba_chiara_moto_noleggio_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'alba_chiara_moto_noleggio_pingback_header' );

/**
 * ACF Gutenberg Blocks
 */
add_action('acf/init', 'my_acf_init');

function my_acf_init() {
	
	if( function_exists('acf_register_block') ) {	
		// register a slideshow block
		acf_register_block(array(
			'name'				=> 'slideshow',
			'title'				=> __('Slideshow home page'),
			'description'		=> __('A custom slideshow block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'images-alt2',
			'keywords'			=> array( 'slideshow', 'carosello' ),
		));
		// register a moped block
		acf_register_block(array(
			'name'				=> 'moped',
			'title'				=> __('Carosello ciclomotori'),
			'description'		=> __('A custom moped block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'admin-network',
			'keywords'			=> array( 'moped', 'ciclomotore' ),
		));
		// register a columns block
		acf_register_block(array(
			'name'				=> 'three-columns',
			'title'				=> __('Testo tripla colonna'),
			'description'		=> __('A custom columns block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'text',
			'keywords'			=> array( 'three columns', 'tripla colonna' ),
		));
		// register a columns block
		acf_register_block(array(
			'name'				=> 'two-columns',
			'title'				=> __('Testo doppia colonna'),
			'description'		=> __('A custom columns block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'text',
			'keywords'			=> array( 'two columns', 'doppia colonna' ),
		));
		// register a full screen row block
		acf_register_block(array(
			'name'				=> 'home-row',
			'title'				=> __('Fascia full screen home page'),
			'description'		=> __('A custom row full screen block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'text',
			'keywords'			=> array( 'home row', 'fascia full screen home' ),
		));
		// register a full screen row block
		acf_register_block(array(
			'name'				=> 'rental-row',
			'title'				=> __('Fascia full screen noleggio'),
			'description'		=> __('A custom row full screen block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'text',
			'keywords'			=> array( 'rental row', 'fascia full screen noleggio' ),
		));
		// register a teaser blog block
		acf_register_block(array(
			'name'				=> 'teaser-blog',
			'title'				=> __('Teaser blog'),
			'description'		=> __('A custom teaser blog block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'flag',
			'keywords'			=> array( 'teaser blog', 'teaser blog' ),
		));
		// register a teaser contact block
		acf_register_block(array(
			'name'				=> 'teaser-contact',
			'title'				=> __('Teaser contact'),
			'description'		=> __('A custom teaser contact block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'email',
			'keywords'			=> array( 'teaser contact', 'teaser contattaci' ),
		));
		// register a maps block
		acf_register_block(array(
			'name'				=> 'maps',
			'title'				=> __('Maps'),
			'description'		=> __('A custom maps block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'admin-site-alt',
			'keywords'			=> array( 'Maps', 'Mappa' ),
		));
		// register a contact block
		acf_register_block(array(
			'name'				=> 'contact',
			'title'				=> __('Contact'),
			'description'		=> __('A custom contact block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'email',
			'keywords'			=> array( 'Contact', 'Contatti e form' ),
		));
		// register a paragraph block
		acf_register_block(array(
			'name'				=> 'paragraph',
			'title'				=> __('Paragraph'),
			'description'		=> __('A custom paragraph block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'layout',
			'icon'				=> 'editor-alignleft',
			'keywords'			=> array( 'Paragraph', 'Paragrafo semplice (Alba Chiara)' ),
		));
		acf_update_setting('google_api_key', 'AIzaSyBrCDdwb9CpTMKo50WxZBTpr49i9LEa-gY');
	}
}
function my_acf_block_render_callback( $block ) {	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") );
	}
}

/**
 * Hide editor in template
 */
add_action('init', 'my_rem_editor_from_post_type');
function my_rem_editor_from_post_type() {
    remove_post_type_support( 'ciclomotori', 'editor' );
}

/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
	return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);

/**
 * Edit in blank target
 */
add_filter( 'edit_post_link', function( $link, $post_id, $text )
{
    // Add the target attribute 
    if( false === strpos( $link, 'target=' ) )
        $link = str_replace( '<a ', '<a target="_blank" ', $link );

    return $link;
}, 10, 3 );