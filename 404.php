<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package alba_chiara
 */

get_header();
?>
	<section class="error-404 not-found">
		<header class="page-header">
		</header><!-- .page-header -->

		<div class="page-content">
			<img class="img-error" src="<?php echo get_template_directory_uri() ?>/images/error-404.svg" alt="Alba Chiara Noleggio - Page not found">
			<h3><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'alba-chiara-moto-noleggio' ); ?></h3>
			<a href="<?php echo get_home_url() ?>" class="o-button">HOME</a>
		</div><!-- .page-content -->
	</section><!-- .error-404 -->
<?php

get_footer();
