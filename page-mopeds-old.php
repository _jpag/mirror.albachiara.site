
<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alba_chiara
 * 
 * Template name: Template noleggio ciclomotori
 */
$hero_title = get_field('hero_title');
$hero_subtitle = get_field('hero_subtitle');	
$hero_text = get_field('hero_text');	

$title_link1 = get_field('title_link_1');	
$link1 = get_field('link_1');	
$title_link2 = get_field('title_link_2');	
$link2 = get_field('link_2');	

$image = get_field('image');

$body_title = get_field('body_title');
$body_text = get_field('body_text');	

get_header(); ?>
<section class="c-mopeds__hero" style="background-image:url('<?php echo $image ?>')">
    <div class="c-mopedshero__content l-container">
        <div>
            <?php if ($hero_subtitle): ?>
                <h2 class="c-mopedshero__subtitle"><?php echo $hero_subtitle ?></h2>
            <?php endif ?>  
    
            <?php if ($hero_title): ?>
                <h1 class="c-mopedshero__title"><?php echo $hero_title ?></h1>
            <?php endif ?>  
    
            <?php if ($hero_text): ?>
                <p class="c-mopedshero__text u-left-line"><?php echo $hero_text ?></p>
            <?php endif ?>  
        </div>
        <div class="c-mopedshero__actions">
            <div class="c-mopedshero__link1">
                <?php if ($title_link1): ?>
                    <h3 class="u-left-line"><?php echo $title_link1 ?></h3>
                <?php endif ?>  
                <?php if ($link1): ?>
                    <a class="o-button white" target="<?php echo $link1['target'] ?>" href="<?php echo $link1['url'] ?>"><?php echo $link1['title'] ?></a>
                <?php endif ?>  
            </div>
            <div class="c-mopedshero__link2">
                <?php if ($title_link2): ?>
                    <h3 class="u-left-line"><?php echo $title_link2 ?></h3>
                <?php endif ?>  
                <?php if ($link2): ?>
                    <a class="o-button white" target="<?php echo $link2['target'] ?>" href="<?php echo $link2['url'] ?>"><?php echo $link2['title'] ?></a>
                <?php endif ?>  
            </div>
        </div>
    </div>
</section>
<section class="c-mopeds__body u-deco-square">
    <div class="js-close c-mopedDetail__close"><img src="<?php echo get_template_directory_uri() ?>/images/icn-close.svg" alt=""></div>

    <div class="js-appear" js-delay="50">
        <div class="l-container">
            <?php if ($body_title): ?>
                <h2 class="c-mopedsBody__title"><?php echo $body_title ?></h2>
            <?php endif ?>  
            <?php if ($body_text): ?>
                <?php echo $body_text ?>
            <?php endif ?>  
        </div>
        <div class="c-mopedsbody__inner l-container_mopeds">
            <?php
                $query = new WP_Query( array('posts_per_page' => 99, 'post_type' => 'ciclomotori' ) );
                if ( $query->have_posts() ):
                    while ( $query->have_posts() ) : $query->the_post(); ?>	
                        <div id="moped-<?php the_ID(); ?>" class="c-moped js-mopedRental">
                            <?php echo the_post_thumbnail('medium') ?>
                            <div class="c-moped__name">
                                <?php echo get_the_title() ?>
                            </div>                                
                        </div>
                        <?php
                    endwhile;
                endif;
            ?>
        </div>

    </div>
        <?php
            if ( $query->have_posts() ):
                while ( $query->have_posts() ) : $query->the_post(); 
                    get_template_part( 'template-parts/content', 'moped' );
                endwhile;
            endif;

            the_post(); 
        ?>
    <?php the_content(); ?>
</section>

<?php get_footer(); ?>
