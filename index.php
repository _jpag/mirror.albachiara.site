<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alba_chiara
 */

 $num = 0;

get_header();
?>
	<div class="c-blog l-container">
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				if ($num == 0) {
					get_template_part( 'template-parts/content', 'itemBig' );
				} else {
					get_template_part( 'template-parts/content', 'itemSmall' );
				}
				$num += 1;
			endwhile; 
			else :
				get_template_part( 'template-parts/content', 'none' );
		endif;
		?>
	</div>
	<?php

the_posts_navigation();
get_footer();
