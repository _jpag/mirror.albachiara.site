(function ($) {
    var isMobile = $(window).innerWidth() < 320;
    var showreelInitializer = function () {
        var sl = $(".c-slideshow");
        if (sl.length > 0) {
            sl.cycle({
                timeout: 5000,
                speed: 500,
                slides: '.item',
                swipe: true,
                fx: 'fadeout',
                log: false,
                //swipeFx: 'scrollHorz',
                pager: '.c-slideshow__pager',
                pagerTemplate: '<a href="javascript:;" class="pager"></a>',
                pagerActiveClass: 'isActive'
            });
        }
    };

    var carouselInitializer = function () {
        if ($(window).innerWidth() <= 767) {      
            $('.js-carouselBlog').owlCarousel({
                responsive: {
                    0: {
                        items: 1,
                        margin: 20,
                        stagePadding: 30,
                        loop: true,
                        nav: false,
                        dots: true,
                        center: true,
                    }
                }
            })
        }
        
        $('.js-carouselHome').owlCarousel({
            loop: false,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 20,
                    stagePadding: 30,
                },
                768: {
                    items: 2,
                    margin: 20,
                    stagePadding: 30,
                },
                1024: {
                    items: 3,
                    margin: 40,
                    stagePadding: 50,
                },
                1280: {
                    items: 4,
                    margin: 50,
                    stagePadding: 50,
                },
                1920: {
                    items: 5,
                    margin: 70,
                    stagePadding: 50,
                }
            }
        })
    };

    var togglerMenuMobile = function () {
        $(document).on("click", ".js-menuToggler", function () {
            $('body').toggleClass("openMenu");
        });
    };

    var menuMobile = function (e) {
        var el = $('.js-noPage').children('a');
        var w = $(window).innerWidth();
        if (el.length > 0 && w < 1024) {
            el.on('click', function (e) {
                e.preventDefault();
                el.siblings('.sub-menu').removeClass('isOpen');
                $(this).siblings('.sub-menu').addClass('isOpen');
            });
        }
    };

    var anchorScroll = function () {
        if (isMobile) {
            var topPos = 40;
        } else {
            var topPos = 120;
        }

        $('.js-anchorScroll').click(function () {
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - topPos
            }, 600);
            return false;
        });
    };

    var bodyScroll = function () {
        var n = $('body');

        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            n.toggleClass("isScrolled", scroll > 130);
        });
    };

    var jsAppearInitializer = function () {
        /*
            Per attivare questa animazione basta aggiungere la classe js-appear a qualsiasi elemento.
            Se si vuole aggiungere un delay aggiungere un attributo "js-delay" es js-delay="150"
            (da utilizzare ad es. su elementi sulla stessa fila per non farli comparire tutti in blocco)
        */
        if ($(".js-appear").length > 0 && $(window).innerWidth() > 767) {
            var viewPortHeight = $(window).height();
            var currPosition = $(window).scrollTop();
    
            var isOnScreen = function (el) {
                // console.log('on screen');
                return ($(el).offset().top + 200) < (currPosition + viewPortHeight - ($(el).attr("js-delay") || 0));
            }
            $(".js-appear").each(function () {
                $(this).toggleClass("hidden", !isOnScreen(this));
            });
            var scrollHappened = function () {
                currPosition = $(window).scrollTop();
                $(".js-appear.hidden").each(function () {
                    $(this).toggleClass("hidden", !isOnScreen(this));
                });
            };
    
            $(window).scroll(scrollHappened);
    
            $(window).resize(function () {
                viewPortHeight = $(window).height()
                scrollHappened();
            });
        }
    
    };

    var mopedDetailController = function() {
        var itemHome = '.js-mopedHome';
        var itemRental = '.js-mopedRental';
        var mopedDetail = '.js-mopedDetail';
        var storage = localStorage.getItem('itemName');
        if ($(itemHome).length > 0 || $(itemRental).length > 0) {
               
            if (storage != null) {
                var _storage = localStorage.getItem('itemName');
                _addClassDetail(_storage);
                localStorage.clear();
            }   
            function _addClassDetail(el) {
                $('body').addClass('isOverlay');
                $('.' + el).addClass('isOpen');  
            }

            function openDetail(el) {  
                $(document).on('click', el, function(e) {
                    e.stopPropagation();
                    var _mopedId = $(this).attr('id');
                    _addClassDetail(_mopedId);
                });
            }

            function closeDetail(el) {
                var _closeDetail = '.js-close';
                $(document).on('click', 'body', function(e) {
                    e.stopPropagation();
                    $('body').removeClass('isOverlay');
                    $(el).removeClass('isOpen');
                });
            }

            function openDetailFromHome(el) {
                $(document).on('click', el, function(e){
                    var _nameId = $(this).attr('id');
                    localStorage.setItem(
                        'itemName',
                        _nameId      
                        );
                });
            }

            openDetail(itemRental);
            closeDetail(mopedDetail);
            //openDetailFromHome(itemHome);
        }
    };

    $(document).ready(function () {
        showreelInitializer();
        togglerMenuMobile();
        menuMobile();
        anchorScroll();
        bodyScroll();
        carouselInitializer();
        jsAppearInitializer();
        mopedDetailController();
    })

})(jQuery);