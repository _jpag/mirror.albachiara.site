<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alba_chiara
 */

?>

</div><!-- #content -->

<footer id="colophon" class="c-footer">
	<div class="l-container">
		<div class="c-footer__bottom">
			<div>
				<div class="inner">
					<div>
					<a href="https://www.facebook.com/albachiaranoleggitrieste/" target="_blank">
							<img class="social-icon"
								src="<?php echo get_template_directory_uri() ?>/images/social-facebook.svg"
								alt="Alba Chiara Noleggio Facebook">
						</a>
						<a href="https://www.instagram.com/alba_chiara_noleggi/?hl=it" target="_blank">
							<img class="social-icon"
								src="<?php echo get_template_directory_uri() ?>/images/social-instagram.svg"
								alt="Alba Chiara Noleggio Instagram">
						</a>
					</div>
				</div>
				<div class="inner">
					<p><?php echo date("Y");?>&nbsp;©<?php if (!dynamic_sidebar('footerbottom') ) : endif; ?></p>
				</div>
			</div>
			<a href="http://www.brainupstudio.it" target="_blank">
				<img src="<?php echo get_template_directory_uri() ?>/images/brainup-studio-logo.svg"
					alt="BrainUp Studio - Web design">
			</a>
		</div>
	</div>

</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>